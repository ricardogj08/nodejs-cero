const mongoose = require('../connections/mongodb')
const Schema = mongoose.Schema

const postSchema = new Schema({
  title: String,
  body: String
})

module.exports = mongoose.model('Post', postSchema)
