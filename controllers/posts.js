const Post = require('../models/posts')

module.exports = {
  /**
   * Vista para crear un nuevo post.
   */
  new (req, res) {
    res.render('posts/new', { title: 'Crear un nuevo post' })
  },

  /**
   * Guarda un nuevo post.
   */
  create (req, res) {
    const post = new Post({
      title: req.body.title,
      body: req.body.body
    })

    post.save(err => {
      if (err) {
        console.error(err)
        throw err
      }
    })

    res.redirect('/posts')
  },

  /**
   * Lista de posts.
   */
  index (req, res) {
    Post.find(({}, (err, docs) => {
      if (err) {
        console.error(err)
        throw err
      }

      res.render('posts/index', { title: 'Lista de posts', posts: docs })
    }))
  },

  /**
   * Vista para editar un post.
   */
  edit (req, res) {
    Post.findById(req.params.id, (err, doc) => {
      if (err) {
        console.error(err)
        throw err
      }

      res.render('posts/edit', { title: doc.title, post: doc })
    })
  },

  /**
   * Actualiza la información de un post.
   */
  update (req, res) {
    const id = req.params.id

    Post.findByIdAndUpdate(id, { title: req.body.title, body: req.body.body }, err => {
      if (err) {
        console.error(err)
        throw err
      }
    })

    res.redirect(`/posts/${id}/edit`)
  },

  /**
   * Elimina un post.
   */
  delete (req, res) {
    Post.findByIdAndDelete(req.params.id, err => {
      if (err) {
        console.error(err)
        throw err
      }
    })

    res.redirect('/posts')
  }
}
