const conn = require('../connections/mysql')

module.exports = {
  /**
   * Vista para crear un nuevo usuario.
   */
  new (req, res) {
    res.render('users/new', { title: 'Crear un nuevo usuario' })
  },

  /**
   * Guarda un nuevo usuario.
   */
  create (req, res) {
    const query = 'INSERT INTO users (name, age) VALUES (?, ?)'

    conn.query(query, [req.body.name, req.body.age], (err, results, fields) => {
      if (err) {
        console.error(err.sqlMessage)
        throw err
      }
    })

    res.redirect('/users')
  },

  /**
   * Lista de usuarios.
   */
  index (req, res) {
    const query = 'SELECT * FROM users ORDER BY id ASC'

    conn.query(query, (err, results, fields) => {
      if (err) {
        console.error(err.sqlMessage)
        throw err
      }

      res.render('users/index', { title: 'Lista de usuarios', users: results })
    })
  },

  /**
   * Vista para editar un usuario.
   */
  edit (req, res) {
    const query = 'SELECT * FROM users WHERE id = ? LIMIT 1'

    conn.query(query, [req.params.id], (err, results, fields) => {
      if (err) {
        console.error(err.sqlMessage)
        throw err
      }

      res.render('users/edit', { title: results[0].name, user: results[0] })
    })
  },

  /**
   * Actualiza la información de un usuario.
   */
  update (req, res) {
    const id = req.params.id

    const query = 'UPDATE users SET name = ?, age = ? WHERE id = ?'

    conn.query(query, [req.body.name, req.body.age, req.params.id], (err, results, fields) => {
      if (err) {
        console.error(err.sqlMessage)
        throw err
      }
    })

    res.redirect(`/users/${id}/edit`)
  },

  /**
   * Elimina un usuario.
   */
  delete (req, res) {
    const query = 'DELETE FROM users WHERE id = ?'

    conn.query(query, [req.params.id], (err, results, fields) => {
      if (err) {
        console.error(err.sqlMessage)
        throw err
      }
    })

    res.redirect('/users')
  }
}
