const express = require('express')
const router = express.Router()
const userController = require('../controllers/users')

router.get('/new', userController.new)
router.post('/', userController.create)
router.get('/', userController.index)
router.get('/:id/edit', userController.edit)
router.post('/:id', userController.update)
router.get('/:id', userController.delete)

module.exports = router
