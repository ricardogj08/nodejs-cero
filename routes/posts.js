const express = require('express')
const router = express.Router()
const postController = require('../controllers/posts')

router.get('/new', postController.new)
router.post('/', postController.create)
router.get('/', postController.index)
router.get('/:id/edit', postController.edit)
router.post('/:id', postController.update)
router.get('/:id', postController.delete)

module.exports = router
