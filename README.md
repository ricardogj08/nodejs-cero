# Node.js desde cero - Introducción

Notas del curso de Node.js Desde Cero en EDteam.

## Notas

* `nodemon` - Es un paquete que recarga automáticamente un servidor Node.js cuando hay cambios en un proyecto.
* `npm install -D package` - Instala dependencias de desarrollo.
* `scripts` - Es un campo del archivo `package.json` que permite agregar comandos personalizados con npm.
* `npm run serve` - Ejecuta nodemon.
* <http://localhost:3000>
* `Mongoose` - Es un ORM de Node.js para MongoDB.

## Referencias

* [nodemon](https://nodemon.io/)
* [EJS](https://ejs.co/)
* [mysql](https://github.com/mysqljs/mysql)
* [MongoDB Community Server](https://www.mongodb.com/try/download/community)
* [MongoDB Shell](https://www.mongodb.com/try/download/shell)
* [Mongoose](https://mongoosejs.com/)
