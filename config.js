module.exports = {
  mysql: {
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'nodejs',
    charset: 'utf8mb4_spanish_ci'
  },
  mongodb: {
    host: 'localhost',
    user: '',
    password: '',
    database: 'nodejs',
    port: 27017
  }
}
