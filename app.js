const express = require('express')
const app = express()
const path = require('path')
const port = 3000

/**
 * Settings.
 */
app.set('view engine', 'ejs')

/**
 * Middlewares.
 */
app.use(express.urlencoded({ extended: false }))
app.use(express.static(path.join(__dirname, 'public')))

/**
 * Rutas.
 */
app.use('/', require('./routes/home'))
app.use('/users', require('./routes/users'))
app.use('/posts', require('./routes/posts'))

app.listen(port, () => {
  console.log(`Express listening on port ${port}`)
})
