const mongoose = require('mongoose')
const config = require('../config').mongodb

mongoose.connect(`mongodb://${config.host}:${config.port}/${config.database}`).then(
  () => {
    console.log('MongoDB: Connection successful')
  },
  err => {
    console.log(err)
  }
)

module.exports = mongoose
