const mysql = require('mysql')
const connection = mysql.createConnection(require('../config').mysql)

connection.connect((err) => {
  if (err) {
    console.error('error connecting: ' + err.sqlMessage)
    return
  }

  console.log('connected as id ' + connection.threadId)
})

module.exports = connection
